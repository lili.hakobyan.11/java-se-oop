package com.lilit.hakobyan.exercises;

import java.util.Scanner;

public class Exercises {

    public void  Ex1() {
        System.out.println("Exercise 1: Printing - Hello and my name on a separate line\nHello\nLilit" );
    }
    public void Ex2(){
        System.out.println("Exercise 2: Printing  the sum of two numbers");
        int number1 = 74;
        int number2 = 36;
        System.out.println(number1+number2);
    }
    public void Ex3(){
        System.out.println("Exercise 3: Printing  the division of two numbers");
        int number1 = 50;
        int number2 = 3;
        System.out.println(number1/number2);
    }
    public void Ex4(){
        System.out.println("Exercise 4: Printing  the  result of given operations");
        System.out.println(-5+8*6);
        System.out.println((55+9)%9);
        System.out.println(20+-3*5/8);
        System.out.println(5+15/3*2-8%3);
    }
    public void Ex5(){
        System.out.println("Exercise 5: Taking two numbers as input and displaying the product of two numbers");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input the first number:");
        int input1 = scanner.nextInt();
        System.out.println("Input the second number");
        int input2 = scanner.nextInt();

        System.out.println(input1 + " * " + input2 + " = " + input1*input2 );
    }
    public void Ex6(){
        System.out.println("Exercise 6: printing the given operation with two inputs");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input the first number:");
        int input1 = scanner.nextInt();
        System.out.println("Input the second number");
        int input2 = scanner.nextInt();

        System.out.println(input1 + " + " + input2 + " = " + (input1+input2) );
        System.out.println(input1 + " - " + input2 + " = " + (input1-input2) );
        System.out.println(input1 + " * " + input2 + " = " + input1*input2 );
        System.out.println(input1 + " / " + input2 + " = " + input1/input2 );
        System.out.println(input1 + " mod " + input2 + " = " + input1%input2);
    }
    public void Ex7(){
        System.out.println("Exercise 7: printing the multiplication table upto 10 of taken input");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input the number:");
        int  input = scanner.nextInt();
        for (int i = 1; i < 11; i++){
            System.out.println(input + "*" + i + "=" + input*i);
        }

    }
    public void Ex8(){
        System.out.println("Exercise 8: Shuffling a given array");

    }


}
